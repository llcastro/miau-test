FROM ubuntu:16.04

WORKDIR /app
COPY ./dist .

RUN cp miau-test /usr/local/bin

CMD NODE_URLS=https://*:$PORT miau-test

#ENTRYPOINT [ "miau-test" ]