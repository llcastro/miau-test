const express = require('express');
const jwt = require('jsonwebtoken');
const User = require('../models/User');
const auth = require('../middleware/auth');

const router = express.Router();

router.post('/user', auth, async (req, res) => {
  try {
    const user = await User.saveUserAndAddresses(req.body);
    res.status(200).send({ user });
  } catch (error) {
    res.status(400).send(error);
  }
});

router.get('/users', auth, async (req, res) => {
  try {
    const limit = +req.query.limit > 0 ? +req.query.limit : 5;
    const page = +req.query.page >= 0 ? +req.query.page : 1;
    const result = await User.find({})
                              .populate({ path: 'addresses', match: { is_deleted : { $ne: true }}})
                              .where({is_deleted : { $ne: true }}) // $ne = not equal
                              .limit(limit)
                              .skip((page - 1) * limit)
                              .sort('nome');
    res.status(200).send(result);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.get('/user/filter', auth, async (req, res) => {
  try {
    const nome = req.query.nome;
    let data_cadastro_de = new Date();
    data_cadastro_de.setDate(data_cadastro_de.getDate() - 30);
    let data_cadastro_ate = new Date();
    if (req.query.data_cadastro_de && req.query.data_cadastro_de != '') {
      const temp = req.query.data_cadastro_de.split('/');
      data_cadastro_de = new Date(temp[2], temp[1] - 1, temp[0]);
    }
    if (req.query.data_cadastro_ate && req.query.data_cadastro_ate != '') {
      const temp = req.query.data_cadastro_ate.split('/');
      data_cadastro_ate = new Date(temp[2], temp[1] - 1, temp[0]);
    }
    data_cadastro_de = new Date(data_cadastro_de.getFullYear(),
                                data_cadastro_de.getMonth(),
                                data_cadastro_de.getDate(),
                                0,
                                0,
                                0);
    data_cadastro_ate = new Date(data_cadastro_ate.getFullYear(),
                                 data_cadastro_ate.getMonth(),
                                 data_cadastro_ate.getDate(),
                                 23,
                                 59,
                                 59);
    const limit = +req.query.limit > 0 ? +req.query.limit : 5;
    const page = +req.query.page >= 0 ? +req.query.page : 1;
    const result = await User.find({ nome: { $regex: new RegExp(nome, "ig") }}) // insensitive with global
                              .populate({ path: 'addresses', match: { is_deleted : { $ne: true }}})
                              .where({is_deleted : { $ne: true }}) // $ne = not equal
                              .where({creation_time: {$gte: data_cadastro_de, $lte: data_cadastro_ate}})
                              .limit(limit)
                              .skip((page - 1) * limit)
                              .sort('nome');
    res.status(200).send(result);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.get('/user/:user_id', auth, async (req, res) => {
  try {
    const id = req.params.user_id;
    if (!id.match(/^[0-9a-fA-F]{24}$/)) {
      return res.status(400).send({error: 'ID inválido!'});
    }
    const result = await User.findById(id)
                              .populate({ path: 'addresses', match: { is_deleted : { $ne: true }}})
                              .where({is_deleted : { $ne: true }});
    if (!result) {
      return res.status(400).send({error: 'ID inexistente na base!'});
    }
    res.status(200).send(result);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.patch('/user/:user_id', auth, async (req, res) => {
  try {
    const id = req.params.user_id;
    if (!id.match(/^[0-9a-fA-F]{24}$/)) {
      return res.status(400).send({error: 'ID inválido!'});
    }
    const user_base = await User.findById(id)
                              .populate({ path: 'addresses', match: { is_deleted : { $ne: true }}})
                              .where({is_deleted : { $ne: true }});
    if (!user_base) {
      return res.status(400).send({error: 'ID inexistente na base!'});
    }

    const user = await User.patchUser(user_base, req.body);
    res.status(200).send({ user });
  } catch (error) {
    if (error.message)
      res.status(400).send(error.message);
    else
      res.status(400).send(error);
  }
});

router.delete('/user/:user_id', auth, async (req, res) => {
  try {
    const id = req.params.user_id;
    if (!id.match(/^[0-9a-fA-F]{24}$/)) {
      return res.status(400).send({error: 'ID inválido!'});
    }
    await User.softDelete(id);
    res.status(200).send('Usuário deletado com sucesso!');
  } catch (error) {
    res.status(400).send(error);
  }
});

router.post('/login', async (req, res) => {
  try {
    const user = await User.findByCredentials(req.body.email, req.body.password);
    if (!user) {
      return res.status(403).send({error: 'Usuário ou senha inválidos!'});
    }
    const token = await user.generateAuthToken();
    res.cookie('Bearer', token);
    res.status(200).send({'token': token});
  } catch (error) {
    let message = 'Ocorreu um erro ao logar. ';
    if (error && error.message) {
      message += error.message;
    }
    res.status(400).send(message);
  }
});

router.get('/me', async (req, res) => {
  try {
    const decode = await jwt.decode(req.query.token, process.env.JWT_KEY);
    if (!decode) {
      return res.status(403).send({error: 'Token inválido!'});
    }
    const user = await User.findById(decode._id)
                              .populate({ path: 'addresses', match: { is_deleted : { $ne: true }}})
                              .where({is_deleted : { $ne: true }});
    res.status(200).send(user);
  } catch (error) {
    res.status(400).send(error);
  }
});

module.exports = router;