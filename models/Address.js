const mongoose = require('mongoose');

const schema = mongoose.Schema({
  rua: {
    type: String,
    trim: true,
  },
  numero: {
    type: Number,
  },
  cidade: {
    type: String,
    trim: true,
  },
  estado: {
    type: String,
    trim: true,
  },
  creation_time: {
    type: Date,
  },
  update_time: {
    type: Date,
  },
  is_deleted: {
    type: Boolean,
    index: true,
  }
});

schema.pre('save', async function(next) {
  if (this.isModified) {
    this.update_time = new Date().toISOString();
  }
  if (this.isNew) {
    this.creation_time = new Date().toISOString();
  }
  next();
});

schema.statics.saveAddresses = async (addresses, session) => {
  let savedAddress = [];
  const promises = [];
  for (let address of addresses) {
    promises.push(new Promise(async (resolve, reject) => {
      try {
        const adrs = new Address(address);
        await adrs.save({ session });
        savedAddress.push(adrs._id);
        resolve();
      } catch (error) {
        reject(error);
      }
    }));
  }
  await Promise.all(promises);
  return savedAddress;
}

schema.statics.softDelete = async (ids) => {
  const addresses = [];
  const promises = [];
  for (let id of ids) {
    promises.push(new Promise(async (resolve, reject) => {
      try {
        const address = await Address.findById(id);
        if (!address) {
          throw new Error({error: 'ID inexistente na base!'});
        }
        address.is_deleted = true;
        await address.save();
        addresses.push(address);
        resolve();
      } catch (error) {
        reject(error);
      }
    }));
  }
  
  await Promise.all(promises);
  return addresses;
}

const Address = mongoose.model('Address', schema);

module.exports = Address;