const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const validator = require('validator');
const Address = require('../models/Address');

const schema = mongoose.Schema({
  nome: {
    type: String,
    require: true,
    trim: true,
  },
  email: {
    type: String,
    require: true,
    trim: true,
    unique: true,
    lowercase: true,
    validate: value => {
      if (!validator.isEmail(value)) {
        throw new Error('E-mail inválido!');
      }
    }
  },
  password: {
    type: String,
    require: true,
    minLength: 8,
  },
  addresses: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Address',
  }],
  creation_time: {
    type: Date,
  },
  update_time: {
    type: Date,
  },
  is_deleted: {
    type: Boolean,
    index: true,
  }
});

schema.pre('save', async function(next) {
  if (this.isModified('password')) {
    this.password = await bcrypt.hash(this.password, 8);
  }
  if (this.isModified) {
    this.update_time = new Date().toISOString();
  }
  if (this.isNew) {
    this.creation_time = new Date().toISOString();
  }
  next();
});

schema.methods.generateAuthToken = async function() {
  return jwt.sign({_id: this._id}, process.env.JWT_KEY);
}

schema.statics.softDelete = async (id) => {
  const session = await mongoose.startSession();
  session.startTransaction();
  try {
    const user = await User.findById(id);
    if (!user) {
      throw new Error({error: 'ID inexistente na base!'});
    }
    user.is_deleted = true;
    await user.save();
    await Address.softDelete(user.addresses);

    await session.commitTransaction();
    return user;
  } catch (error) {
    await session.abortTransaction();
    throw new Error({ error });
  } finally {
    session.endSession();
  }
}

schema.statics.findByCredentials = async (email, password) => {
  const user = await User.findOne({ email }).where({is_deleted : { $ne: true }});
  if (!user) {
    throw new Error('Usuário não encontrado!');
  }
  const isPasswordMatch = await bcrypt.compare(password, user.password);
  if (!isPasswordMatch) {
    throw new Error('Usuário/senha inválido!');
  }
  return user;
}

schema.statics.saveUserAndAddresses = async (body) => {
  const session = await mongoose.startSession();
  session.startTransaction();
  try {
    const addresses = await Address.saveAddresses(body.addresses, session);
    body.addresses = [];
    const user = new User(body);
    user.addresses = addresses;
    await User.findOneAndDelete({ email: user.email, is_deleted: true });
    await user.save();

    await session.commitTransaction();
    return user;
  } catch (error) {
    await session.abortTransaction();
    throw new Error({ error });
  } finally {
    session.endSession();
  }
}

schema.statics.patchUser = async (user, body) => {
  const session = await mongoose.startSession();
  session.startTransaction();
  try {
    if (body.nome && body.nome != '') {
      user.nome = body.nome;
    }
    if (body.email && body.email != '') {
      user.email = body.email;
    }
    await user.save();

    await session.commitTransaction();
    return user;
  } catch (error) {
    await session.abortTransaction();
    if (error.message)
      throw new Error(error.message);
    else
      throw new Error({ error });
  } finally {
    session.endSession();
  }
}

const User = mongoose.model('User', schema);

module.exports = User;