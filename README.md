# miau-test

## Aplicação

- Foi realizado um deploy da aplicação no Heroku, está disponível em [https://miau-test.herokuapp.com]
- A Documentação fica em [https://miau-test.herokuapp.com/api-docs]
- Para utilizar a aplicação é necessário logar na aplicação acionando a rota ```/login``` com os dados de usuário e senha (teste@teste.com 12345678), essa rota irá setar o token em um cookie na aplicação que disponibilizará a execução das outras rotas.

- Para rodar a aplicação é necessário ter um arquivo com as variáveis de ambiente na raiz da aplicação com o nome de ```.env```
- O arquivo ```.env``` tem que estar no formato apresentado abaixo
```
PORT=9229
MONGODB_URL="<mongodb url>"
JWT_KEY="jwt key"
```

## Rota da API

A rota para a API padrão é ```/api/v1```

## Rota da documentação

A rota para a API padrão é ```/api-docs```

## Rotas
### GET ```/users```
#### parâmetros

- query param ```limit``` do tipo inteiro não obrigatório, limite de usuários de retorno, (default 5)
- query param ```page``` do tipo inteiro não obrigatório, número da página, (default 1)

#### retorno

- Retorna uma lista de ```User```

#### ação
Retorna os usuários

### POST ```/user```
#### parâmetros

- recebe um usuário do tipo ```CreateUser``` no ```body``` da requisição

#### retorno

- Retorna o usuário criado do tipo ```User```

#### ação
Cria um novo usuário

### POST ```/login```
#### parâmetros

- recebe um usuário do tipo ```LoginUser``` no ```body``` da requisição

#### retorno

- Retorna um token no formato de ```Token```

#### ação
Realiza o login do usuário

### GET ```/me```
#### parâmetros

- query param ```token``` do tipo ```string```

#### retorno

- Retorna o usuário do token no formato de ```User```

#### ação
Retorna o usuário do token

### GET ```/user/:user_id```
#### parâmetros

- path param ```user_id``` do tipo ```string``` obrigatório

#### retorno

- Retorna o usuário do ```user_id``` no formato de ```User```

#### ação
Retorna o usuário do ```user_id```

### DELETE ```/user/:user_id```
#### parâmetros

- path param ```user_id``` do tipo ```string``` obrigatório

#### retorno

- Retorna uma string dizendo que o usuário foi deletado

#### ação
Deleta o usuário do ```user_id``` (SoftDelete)

### PATCH ```/user/:user_id```
#### parâmetros

- path param ```user_id``` do tipo ```string``` obrigatório
- dados a serem alterados no formato de ```PatchUser``` passados no corpo da requisição (não é necessário passar todos os dados de ```PatchUser```)

#### retorno

- Retorna o usuário do ```user_id``` com os dados atualizados

#### ação
Atualiza os dados do ```user_id``` com os valores passados no corpo da requisição

### GET ```/user/filter```
#### parâmetros

- query param ```nome``` do tipo ```string``` não obrigatório
- query param ```data_cadastro_de``` do tipo ```string``` não obrigatório no formato ```DD/MM/YYYY```, (default data atual menos 30 dias)
- query param ```data_cadastro_ate``` do tipo ```string``` não obrigatório no formato ```DD/MM/YYYY```, (default data atual)
- query param ```limit``` do tipo inteiro não obrigatório, limite de usuários de retorno, (default 5)
- query param ```page``` do tipo inteiro não obrigatório, número da página, (default 1)

#### retorno

- Retorna uma lista de ```User```

#### ação
Retorna os usuários que se encaixam nos filtros

## Definições

```User```
```JSON
{
  "password": {
    "type": "string"
  },
  "email": {
    "type": "string"
  },
  "nome": {
    "type": "string"
  },
  "creation_time": {
    "type": "string"
  },
  "update_time": {
    "type": "string"
  },
  "addresses": {
    "type": "array",
    "items": {
      "$ref": "#/definitions/Address"
    }
  }
}
```

```Address```
```JSON
{
  "rua": {
    "type": "string"
  },
  "numero": {
    "type": "integer",
    "format": "int64"
  },
  "cidade": {
    "type": "string"
  },
  "estado": {
    "type": "string"
  }
}
```

```CreateUser```
```JSON
{
  "password": {
    "type": "string"
  },
  "email": {
    "type": "string"
  },
  "nome": {
    "type": "string"
  },
  "addresses": {
    "type": "array",
    "items": {
      "$ref": "#/definitions/Address"
    }
  }
}
```

```PatchUser```
```JSON
{
  "email": {
    "type": "string"
  },
  "nome": {
    "type": "string"
  }
}
```

```LoginUser```
```JSON
{
  "password": {
    "type": "string"
  },
  "email": {
    "type": "string"
  }
}
```

```Token```
```JSON
{
  "token": {
    "type": "string"
  }
}
```