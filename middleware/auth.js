const jwt = require('jsonwebtoken');
const User = require('../models/User');

const auth = async (req, res, next) => {
  const token = req.cookies['Bearer'];
  const data = jwt.verify(token, process.env.JWT_KEY);
  try {
    const user = await User.findById(data._id).where({is_deleted : { $ne: true }});
    if (!user) {
      throw new Error();
    }
    next();
  } catch (error) {
      res.status(403).send({ error: 'Sem acesso a este recurso!' });
  }
}

module.exports = auth;